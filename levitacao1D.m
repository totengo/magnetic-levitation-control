syms x1 x2 x3 u L R c g m v

Sys=[x2; g - (c/m)*(x3/x1)^2; (1/L)*(u-R*x3)]

% A=[0, 1, 0; -1, 0, 0; 0, 0, 0]
% B=[0;-sin(t);1]
% dB=[0; -cos(t); 0]
% d2B=[0; sin(t); 0]

% C=[B, A*B-dB, (A^2)*B-2*A*dB+d2B]

F=x1;

FL0=gradient(F, [x1 x2 x3]); FL0=transpose(FL0)
dF=collect(simplify(FL0*[Sys]), [x1 x2 x3]) 
FL0=FL0(:, 1:3)

FL1=transpose(gradient(dF, [x1 x2 x3]))
d2F=collect(simplify(FL1*[Sys]), [x1 x2 x3]) 
FL1=FL1(:, 1:3)

FL2=transpose(gradient(d2F, [x1 x2 x3]))
d3F=collect(simplify(FL2*[Sys]), [x1 x2 x3]) 
FL3=transpose(gradient(d3F, [x1 x2 x3 u]))
FL3(4)
exp=collect(simplify((v-d3F+FL3(4)*u)/FL3(4)), [x1 x2 x3])



% M=[FL0 0; FL1 0; FL2 0; FL3]

